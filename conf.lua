-- chikun :: 2015
-- The configuration file for Rapture Slumber Party


require "src/flags"

function love.conf(game)

    game.window.width   = 256 * 4
    game.window.height  = 192 * 4

    game.window.title   = "ＧＨＯＳＴ  ＤＵＣＫ"
    game.window.resizable = true
    game.window.fullscreen = false

    game.window.vsync = false

end
