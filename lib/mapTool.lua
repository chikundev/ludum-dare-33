-- chikun :: 2014
-- Tiled .lua map loading tool


-- Table containing map functions
map = {
    current = nil
}


-- Loads and returns a map
function map.load(mapName)

    colLoop = 0

    -- Load map into a table
    local mapData = require("maps/" .. mapName)

    -- New map structure
    local newMap = {
        layers    = { },
        quads     = { },
        tilesets  = { },
        w       = mapData.width,
        h       = mapData.height,
        tileW   = mapData.tilewidth,
        tileH   = mapData.tileheight,
        name    = mapName,
        objects = { }
    }

    -- Loads all quads from the map data
    for key, extTileset in ipairs(mapData.tilesets) do
        -- Our new tileset <3
        local n = { }

        -- Interpret tileset's image into an actual loaded image
        n.image = getResourceFromString(extTileset.image)

        -- Transfer certain values to the new tileset
        n.gid     = extTileset.firstgid
        n.imageW  = extTileset.imagewidth
        n.imageH  = extTileset.imageheight
        n.margin  = extTileset.margin
        n.spacing = extTileset.spacing
        n.tileW   = extTileset.tilewidth
        n.tileH   = extTileset.tileheight

        -- Determine quad value range
        local repeatX = math.ceil((n.imageW - n.spacing) / n.tileW) - 1
        local repeatY = math.ceil((n.imageH - n.spacing) / n.tileH) - 1
        local yPos    = n.margin

        -- Start counting at current gid
        local gidQ = n.gid

        -- Interpret quads from the tileset
        for currentY = 0, repeatY do
            -- Reset x starting position
            local xPos = n.margin

            for currentX = 0, repeatX do
                -- Add current quad to the table
                newMap.quads[gidQ] =
                    g.newQuad(xPos + (currentX * n.tileW), yPos + (currentY * n.tileH),
                    n.tileW, n.tileH, n.imageW, n.imageH)

                -- Increment x starting position
                xPos = xPos + n.spacing

                -- Increment quad number
                gidQ = gidQ + 1
            end

            -- Increment y starting position
            yPos = yPos + n.spacing
        end

        -- Add to the list of map tilesets
        table.insert(newMap.tilesets, n)
    end


    -- Load layers into tables
    for key, layer in ipairs(mapData.layers) do
        -- Create new table based on layer name
        local newLayer   = {
            data    = layer.data,
            name    = layer.name,
            type    = layer.type,
            vars    = layer.properties,
            w       = layer.width,
            h       = layer.height
        }

        -- Create table for objects
        local newObjects = { }

        -- If layer isn't tilelayer, import objects
        if layer.type ~= "tilelayer" then
            for key, object in ipairs(layer.objects) do
                -- Get data from object's quad, if available
                local quadVars = { }

                if object.gid then
                    if object.gid > 1000 then
                        object.gid = nil
                    else
                        quadVars = {newMap.quads[object.gid]:getViewport()}
                    end
                end

                -- Add object to table
                newObjects[#newObjects+1] = {
                    name = object.name,
                    type = object.type,
                    gid = object.gid,
                    w   = (quadVars[3] or object.width),
                    h   = (quadVars[4] or object.height),
                    x   = object.x,
                    y   = object.y - (quadVars[4] or 0),
                    ox  = object.x,
                    oy  = object.y - (quadVars[4] or 0),
                    properties = object.properties,
                    to = object.properties["to"]
                }
            end
        end

        -- Add new layer to mapData

        if layer.name == "door" then

            newMap.doors = newObjects

        elseif layer.name == "fg" then

            newMap.fg = newObjects

        elseif layer.name == "objects" then

            for k, o in ipairs(newObjects) do

                local objectType = objectTypes[o.name] or objectTypes.lard

                table.insert(newMap.objects, objectType(o.x, o.y, o.w, o.h))

            end

        else

            newLayer.objects = newObjects

            table.insert(newMap.layers, newLayer)

        end
    end
    -- Return the newly created map
    return newMap

end


-- Draws the current or given map
function map.draw(givenMap)

    colLoop = colLoop + 60 * love.timer.getDelta()

    if colLoop >= 360 then colLoop = colLoop - 360 end

    -- If map isn't given, default to current
    local u = givenMap or map.current

    -- Draw all layers
    for key, layer in ipairs(u.layers) do

        -- If tile layer...
        if layer.type == "tilelayer" then

            -- ...then draw all tiles in layer
            for key, tile in ipairs(layer.data) do

                if tile > 0 then

                    -- Calculated placement of tile
                    local tmpY = math.floor((key - 1) / u.w)
                    local tmpX = key - (tmpY * u.w) - 1

                    local quadVars = {u.quads[tile]:getViewport()}

                    local tileTmp = {
                        x = tmpX * u.tileW,
                        y = tmpY * u.tileH - quadVars[4] + u.tileH,
                        w = 32,
                        h = 32
                    }

                    -- If tile exists, draw it
                    if tile > 0 then
                        g.draw(getImageFromGID(tile, u), u.quads[tile],
                            tmpX * u.tileW, tmpY * u.tileH - quadVars[4] + u.tileH)
                    end

                end

            end
        else
            -- ...draw all objects in layer...
            for key, object in ipairs(layer.objects) do
                -- ...if that's possible
                if object.gid then
                    g.draw(getImageFromGID(object.gid, u), u.quads[object.gid], object.x, object.y)
                end
            end
        end

    end

end


function map.drawFG(givenMap)

    local map = givenMap or map.current

    if not map.fg then return 0 end

    -- ...draw all objects in layer...
    for key, object in ipairs(map.fg) do
        -- ...if that's possible
        if object.gid then
            g.draw(getImageFromGID(object.gid, map), map.quads[object.gid], object.x, object.y)
        end
    end
end



function map.change(newMap, objectSpawn)

    global.nextMap = newMap

    global.spawnAt = objectSpawn

    state.load(states.fade)

end


-- Set current map
function map.swap(newMap, objectSpawn)

    -- Set our map to the test map
    map.current = newMap

    -- Our new objects table
    objects = { }

    -- Our new collisions table
    collisions = { }

    -- Layers to delete
    layersToDelete = { }

    if (not player.x) then

        player.set(340, 48, 64, 16)

    end

    -- This is how we interpret map data
    objects = map.current.objects

    for i=#layersToDelete, 1, -1 do

        table.remove(map.current.layers, layersToDelete[i])

    end


    -- If given a position to go to...
    if objectSpawn then

        for key, door in ipairs(map.current.doors) do

            if door.to == objectSpawn then

                player.x = door.x + door.w / 2 - 15
                player.y = door.y + door.h / 2

            end
        end
    end

end


-- Returns the image used by a certain gid
function getImageFromGID(gid, givenMap)

    -- If map isn't given, default to current
    local u = givenMap or map.current

    -- Count current number of tilesets
    local num = #u.tilesets

    -- Seek backwards through tilesets until desired image found
    while (gid < u.tilesets[num].gid) do
        num = num - 1
    end

    -- Return found image
    return u.tilesets[num].image

end
