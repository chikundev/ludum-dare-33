-- chikun :: 2014
-- Loads all objects from the /obj folder
-- Loads all items from the /items folder


-- Recursively checks a folder for objects
-- and adds any found to a state.list table
function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- Remove ".lua" extension on file
            local name = val:gsub(".lua", "")

            -- Load state into table
            tab[name] = require(dir .. "/" .. name)

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the obj folder into the obj table
obj  = checkFolder("obj")

-- Load the items folder into the items table
items  = checkFolder("items")

-- Kills checkFolder
checkFolder = nil
