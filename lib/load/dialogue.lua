-- chikun :: 2014
-- Loads all dialogue from the /talk folder


-- Recursively checks a folder for states
-- and adds any found to a dialogue table
function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- Remove ".lua" extension on file
            local name = val:gsub(".lua", "")

            -- Load state into table
            tab[name] = require(dir .. "/" .. name)

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the talk folder into the dialogue table
dialogue = checkFolder("talk")

-- Kills checkFolder
checkFolder = nil
