-- chikun :: 2015
-- A tutorial covering the specific chikun LÖVE libraries


--[[
    Here we will load the chikun specific libaries
  ]]
require "lib/bindings"      -- Very important that you read this file
require "src/flags"         -- Load flags

-- Set default scaling mode
g.setDefaultFilter('nearest', 'nearest', 0)

require "lib/class"
require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]
require "src/objects"
require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/obj"      -- Load level objects and items to obj table
require "lib/load/maps"     -- Load all maps from map folder to maps table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table
require "lib/load/dialogue"

require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this
require "src/npcs"          -- Load all npcs
require "src/cursors"       -- Load all cursor objects
require "src/gui"           -- Load the GUI
require "src/time"
require "src/items"         -- Load the item functions
require "src/player"        -- Load player object and functions
require "src/rooms"         -- Load all tools
require "src/speakers"      -- Load all speakers
require "src/tools"         -- Load all tools
require "src/winChecks"     -- Load win checks


-- Performed at game startup
function love.load()


    -- Make a backup of all maps
    --mapsBackup = deepcopy(maps)


    -- Hide mouse
    m.setVisible(s.getOS() == "Android")


    -- Global variables
    global = {
        scale = 4,
    }

    -- Auto scale window if need
    local _, _, flags = love.window.getMode()

    -- The window's flags contain the index of the monitor it's currently in.
    local width, height = love.window.getDesktopDimensions(flags.display)

    if height < 1000 then

        rescaleTo(2)

    end

    currentTool = "interact"


    -- Cursors
    guiCursor   = { }
    gameCursor  = { }
    cursorImage = "interact"



    click = false

    time = Time()

    bgm.meetup:play()

    -- Set current state to play state
    state.load(states.title)


end


-- DEBUG [REMOVE ON RELEASE]
function love.keypressed(key)

    if key == " " and state.current == states.title and false then

        bgm.meetup:stop()

        time:startHour()
        state.load(states.play)

    end
end



-- Performed on game update
function love.update(dt)


    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15)


    -- Cursor position relevant to GUI
    guiCursor = {
        x = math.round(m.getX() / (g.getWidth() / 256)),
        y = math.round(m.getY() / (g.getHeight() / 192)),
        w = 1,
        h = 1
    }


    -- IF view exists, we're in game and can update gameCursor
    if (view) then

        -- Cursor position relevant to game screen
        gameCursor = {
            x = guiCursor.x,
            y = guiCursor.y - 16,
            w = 1,
            h = 1
        }

    end


    -- Update current state
    state.current:update(dt)


end



-- Performed on game draw
function love.draw()

    g.scale(g.getWidth() / 256, g.getHeight() / 192)


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)


    -- Draw current state
    state.current:draw()


    -- Draw cursor, if not on intro state
    if (state.current ~= states.splash) and (s.getOS() ~= "Android") then

        g.setColor(255, 255, 255)

        local cur = cursor[cursorImage]

        g.draw(cur.image, guiCursor.x - cur.offsetX, guiCursor.y - cur.offsetY)

    end


end



-- Performed to rescale the screen
function rescaleTo(scaleValue)


    -- Set scale value
    global.scale = scaleValue


    -- Set window size
    w.setMode(256 * scaleValue, 192 * scaleValue)


end
