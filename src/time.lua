-- chikun :: 2015
-- Manage the time systems


Time = class(function(timeClass)

        timeClass.hour = 12
        timeClass.minutes = 0

        timeClass.hourEnded = false

        timeClass.deathYet = false

    end)


function Time:update(dt, hmm)

    self.minutes = (self.minutes + dt * (12 / 16))

    if self.minutes > 55 and not self.hourEnded then

        if self.hour == 2 and crystal:isAlive() then

            crystal.escaped = true

            for key, object in ipairs(maps.room9.objects) do

                if object.name == "Balcony" then

                    object.image = gfx.balconyRailSheetCut

                    object.desc = "Sheet pulled off"

                end
            end

        elseif self.hour == 4 then

            for key, object in ipairs(maps.room6.objects) do

                if object.name == "Microwave" then

                    object.image = gfx.microwaveOff

                end
            end
        end

        self.hourEnded = true

        self:endHour()

    elseif self.minutes >= 60 then

        for key, b in pairs(bgm) do

            b:stop()

        end

        bgm.meetup:play()

        toMeeting = true

        if not hmm then

            state.load(states.fadeMeeting)

        end

    end

end


function Time:startHour()
    bgm.meetup:stop()

    self.deathYet = false

    self.hourEnded = false

    if self.hour == 12 then

        bgm.hour1:play()

        amy:setDestination("room10")
        chiyo:setDestination("room5")
        crystal:setDestination("room2")
        melanie:setDestination("room6")
        skylar:setDestination("room7")

    elseif self.hour == 1 then

        openAttic()
        bgm.hour4:play()

        amy:setDestination("room5")
        chiyo:setDestination("room14")
        crystal:setDestination("room11")
        melanie:setDestination("room12")
        skylar:setDestination("room13")

    elseif self.hour == 2 then

        takeSheets()
        bgm.hour3:play()

        for key, object in ipairs(maps.room9.objects) do

            if object.name == "Balcony" then

                object.image = gfx.balconyRailSheet

                object.desc = "Sheet tied on"

            end
        end

        amy:setDestination("room6")
        chiyo:setDestination("room12")
        crystal:setDestination("room9")
        melanie:setDestination("room5")
        skylar:setDestination("room2")

    elseif self.hour == 3 then

        bgm.hour2:play()

        amy:setDestination("room2")
        chiyo:setDestination("room6")
        melanie:setDestination("room3")
        skylar:setDestination("room13")

    elseif self.hour == 4 then

        bgm.hour1:play()
        if not chairSet then

            otaku.escaped = false
            otaku.sequence = {
                "room5",
                "room4",
                "room6"
            }

        end

        amy:setDestination("room13")
        chiyo:setDestination("room10")
        melanie:setDestination("room12")
        skylar:setDestination("room11")

    elseif self.hour == 5 then

        bgm.hour4:play()

        amy:setDestination("room12")
        chiyo:setDestination("room14")
        melanie:setDestination("room3")
        skylar:setDestination("room7")

    end
end


function Time:endHour()

    for key, npc in ipairs(npcs) do

        npc:setDestination("room4")

    end

end
