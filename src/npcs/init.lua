-- chikun :: 2015
-- Load all NPC classes

npc = class(function(newNPC, name, x, y, room, sequence, colour)

        newNPC.colour = colour

        newNPC.name = name or ""
        newNPC.gName = (name:sub(1, 1)):upper() .. name:sub(2)
        newNPC.still = {
            gfx.npcs[newNPC.name].walk1
        }

        newNPC.walk = {
            gfx.npcs[newNPC.name].walk2,
            gfx.npcs[newNPC.name].walk3
        }
        newNPC.step = 0

        newNPC.x = x or 64
        newNPC.y = y or 128

        newNPC.oX = newNPC.x
        newNPC.oY = newNPC.y

        newNPC.room = room or "room4"

        newNPC.sequence = sequence or { }

        newNPC.alive = true
        newNPC.escaped = false

        newNPC.gfx = newNPC.walk

        newNPC.textTimer = 0

    end)


function npc:update(dt)

    if map.current.name == self.room and not self.goal and
       self.room ~= "room4" then

        if not self.text then

            self.text = self:getText()

            self.textStep = 1

        end

        if self.textStep <= #self.text then

            self.textTimer = self.textTimer + dt

            if self.textTimer > 1 + self.text[self.textStep]:len() / 32 then

                self.textStep = self.textStep + 1

                self.textTimer = 0

                self:nextCheck()

            end

        end

    else

        self.text = nil

        self.textTimer = 0

        self.textStep = 1

    end

    if not self.alive or self.escaped then return 0 end

    if #self.sequence > 0 then

        self.gfx = self.walk

        self.step = (self.step + (dt * 8)) % #self.gfx

        if not self.door then

            for key, door in ipairs(maps[self.room].doors) do

                if door.to == self.sequence[1] then

                    local set = false

                    if self.door then

                        if math.abs(self.goal.x - self.x) >
                           math.abs(door.x + door.w / 2 - self.x) then

                            set = true

                        end

                    else

                        set = true

                    end

                    if set then

                        self.door = door

                        self.goal = {
                            x = door.x + door.w / 2,
                            y = math.min(door.y + door.h, 128)
                        }

                    end
                end
            end
        end

        self:moveToGoal(dt)

        if self.x == self.goal.x and self.y == self.goal.y then

            if not ((self.door.name == "stairLeft" or
                   self.door.name == "stairRight") and
                    maps[self.room].name ~= "room2") then

                self.dest = self.room

                for key, door in ipairs(maps[self.sequence[1]].doors) do

                    if door.to == self.dest then

                        self.x = door.x + door.w / 2
                        self.y = math.min(door.y + door.h, 128)

                    end
                end
            end

            self.door = nil
            self.goal = nil
            self.room = self.sequence[1]

            table.remove(self.sequence, 1)

        end

    else

        if self.room == "room4" then

            self.goal = {
                x = self.oX,
                y = self.oY
            }

        elseif state.current == states.play then

            self.goal = roomStandPos[self.room]

        end

        local still = true

        if self.goal then

            self:moveToGoal(dt)

            self.gfx = self.walk

            self.step = (self.step + (dt * 8)) % #self.gfx

            still = self.x == self.goal.x and self.y == self.goal.y

        end

        if still then

            self.step = 0
            self.gfx = self.still

            self.goal = nil

            self:checkDeath()

        end
    end
end


function npc:moveToGoal(dt)

    local moveX = false

    if self.y >= self.goal.y then

        moveX = (self.x ~= self.goal.x)

    end

    local speed = 40

    if self.name == "crystal" and time.hour == 2 then

        speed = 80

    end

    if moveX then

        self.x = self.x + (math.sign(self.goal.x - self.x) * speed * 2 * dt)

        if math.abs(self.x - self.goal.x) < 4 then

            self.x = self.goal.x

        else

            self.facing = math.sign(self.goal.x - self.x)

        end

    else

        self.y = self.y + (math.sign(self.goal.y - self.y) * speed * dt)

        if math.abs(self.y - self.goal.y) < 4 then

            self.y = self.goal.y

        end

    end

end


function npc:checkDeath()

    if time.deathYet then return 0 end

    if self.room == "room7" and batterySet then

        time.minutes = 60 ; self.alive = false

        state.load(states.deathLamp)

    elseif self.room == "room3" and chanFallen then

        time.minutes = 60 ; self.alive = false

        state.load(states.deathChan)

    elseif self.room == "room14" and ladderGreased then

        time.minutes = 60 ; self.alive = false

        state.load(states.deathLadder)

    elseif self.room == "room9" and sheetsCut then

        time.minutes = 60 ; self.alive = false

        state.load(states.deathFall)

    elseif self.room == "room6" and pillsDone and time.hour == 4 then

        time.minutes = 60 ; self.alive = false

        state.load(states.deathRamen)

    else

        return 0

    end

    time.deathYet = self.name
    time.deathNum = self.num

end


function npc:draw(alpha, ddd)

    if (not alpha and self.escaped) or not self.alive then return 0 end

    if time.hour == 2 and self.room == "room9" then return 0 end

    g.setColor(255, 255, 255, (alpha or 1) * 255)

    if map.current.name ~= self.room and not ddd then return 0 end

    g.draw(self.gfx[math.floor(self.step) + 1], self.x, self.y - 72, 0,
        self.facing, 1, 16)


    -- Overtext
    if map.current.name == self.room and not self.goal and
       self.text and self.room ~= "room4" then

        local text, x =
            self.text[self.textStep] or "",
            self.x - 32

        local y = self.y - 74 - ({fnt.small:getWrap(text, 128)})[2] * 6

        g.setColor(self.colour[1], self.colour[2], self.colour[3])

        g.setFont(fnt.small)

        g.printf(text, x, y, 128, 'center', 0, 0.5)

        g.setColor(0, 0, 0)

        g.setFont(fnt.smallre)

        g.printf(text, x, y, 128, 'center', 0, 0.5)

    end

end


function npc:setDestination(desti)

    local seq = { }

    local dest, room = tonumber(desti:sub(5)), tonumber(self.room:sub(5))

    -- Return to main hall
    if dest == 4 then

        if room == 10 then

            -- do nothing

        elseif room > 8 then

            table.insert(seq, "room10")

        elseif room == 8 then

            table.insert(seq, "room7")

        elseif room == 2 then

            table.insert(seq, "room5")
        end


    elseif dest ~= 4 then

        if dest == 10 then
            --do nothing
        elseif dest > 8 then

            table.insert(seq, "room10")

        elseif dest == 8 then

            table.insert(seq, "room7")

        elseif dest == 2 then

            table.insert(seq, "room5")

        end

    end

    -- If destination is not same room
    if desti ~= self.room then

        table.insert(seq, desti)

    end

    -- Set NPC seqeuence
    self.sequence = seq

    self.door = nil

end


function npc:getItemName()

    return self.item or "None"

end


function npc:isEscaped()

    return self.escaped

end


function npc:isAlive()

    return self.alive

end

require(... .. "/funcs")
require(... .. "/text")

require(... .. "/amy")
require(... .. "/chiyo")
require(... .. "/crystal")
require(... .. "/melanie")
require(... .. "/otaku")
require(... .. "/skylar")


npcs = {
    Amy(320, 128),
    Chiyo(345.6, 128),
    Crystal(371.2, 128),
    Melanie(396.8, 128),
    Skylar(422.4, 128),
    Otaku(32, 128)
}

amy = npcs[1]       ; amy.num = 1
chiyo = npcs[2]     ; chiyo.num = 2
crystal = npcs[3]   ; crystal.num = 3
melanie = npcs[4]   ; melanie.num = 4
skylar = npcs[5]    ; skylar.num = 5
otaku = npcs[6]    ; skylar.num = 6

otaku.escaped = true
