function countHouseNPC()

    local count = 0

    for key, npc in ipairs(npcs) do

        if npc:isAlive() and not npc:isEscaped() then

            count = count + 1

        end

    end

    return count

end


function resetNPCPositions()

    local houseNPCs, k = countHouseNPC(), 0

    for key, npc in ipairs(npcs) do

        if npc:isAlive() and not npc:isEscaped() then

            npc.room = "room4"

            npc.x = 300 + (168 * ((k + 1) / (houseNPCs+1)))
            npc.y = 128

            npc.oX = npc.x
            npc.oY = npc.y


            npc.goal = nil

            npc.sequence = { }

            npc.facing = 1

            if k > houseNPCs / 2 then npc.facing = -1 end

            k = k + 1

        end
    end

end
