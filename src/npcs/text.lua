-- chikun :: 2015
-- Idle text for NPCs

idleText = {
    room2 = {
        "Whoa, what?",
        "There appears to be... another door?",
        "Is music coming from that door?",
        "But it's locked...",
        "I can't open the door.",
        "Pity.",
        "There could be someone in there!",
        "They could help us get out!"
    },
    room3 = {
        "It sure looks elegant in here... I guess.",
        "Pity there's not much food though.",
        "I don't want to starve to death in this place!"
    },
    room5 = {
        "So this is where we entered...",
        "Why is the door locked?",
        "Who... or what... is keeping us here?"
    },
    room6 = {
        "Wait, hold on...",
        "I smell something. Is that... ramen?",
        "Why is ramen in the microwave?",
        "Who even eats ramen?",
        "I'd better not touch it...",
        "But it's still cooking on countdown?",
        "Why is the microwave cooking the ramen for hours?",
        "Won't that... burn it?",
        "I'd better leave."
    },
    room6o = {
        "I love ramen so much.",
        "But I eat so much of it...",
        "It totally can't be healthy.",
        "One day it might kill me..."
    },
    room7 = {
        "Hey, why's it so dark in here?",
        "I can't see a thing!",
        "I think there's a lamp in here?",
        "Let me turn it on...",
        "Oh, it doesn't work."
    },
    room12 = {
        "Look at all these books!",
        "These shelves look somewhat rickety though...",
        "But so many books!",
        "I'm going to read them all.",
        "After all, if we're stuck here for a while...",
        "May as well entertain myself!"
    },
    room9 = {
        "Not much of a view...",
        "If only we could get out from here.",
        "But it's too high up!",
        "Maybe if someone had rope, something sturdy..."
    },
    room10A = {
        "This looks like the same hall as downstairs...",
        "Huh? What's this? It looks like a hatch...",
        "This ladder must lead to the attic.",
        "It's too dark. Maybe we will explore this later.",
        "Besides, that ladder looks dangerous.",
        "What if someone were to slip on it?"
    },
    room10B = {
        "So this ladder must lead to the attic.",
        "I'm not going up there. Too dark.",
        "Besides, that ladder looks dangerous.",
        "What if someone were to slip on it?"
    },
    room11A = {
        "Alright, what's in here...",
        "A bed? Those sheets look nice.",
        "I wonder... could we use this sheet to escape?",
        "Let's take these...",
        "I think I might look for a place to use these.",
        "Perhaps we could climb down the house from up here?"
    },
    room11B = {
        "So this is the bedroom.",
        "That's right... someone's taken the sheets.",
        "I wonder what happened. Did they escape?",
        "I guess it's a mystery."
    },
    room8 = {
        "Are these weapons on the wall?",
        "These look dangerous.",
        "This isn't much of a study..." ,
        "What sort of person lives here?",
        "What kind of person uses weapons?",
        "I have a bad feeling about this place..."
    },
    room13 = {
        "Of all the places to be...",
        "A bathroom.",
        "How pleasant." ,
        "What's in here, anyway?",
        "This cabinet looks suspicious...",
        "There seems to be... pills in here?",
        "What's with that?"
    },
    room14 = {
        "Getting up here was hard work.",
        "All because of that ladder. How unsafe.",
        "Well, there's not much up here.",
        "It's damp.",
        "And dark.",
        "I wish I was anywhere but here."
    }
}


function npc:getText()

    local text = { }

    if self.room == "room10" then

        if atticOpened then

            text = idleText.room10B

        else

            text = idleText.room10A

        end

    elseif self.room == "room11" then

        if sheetAcquired and time.hour~=11 then

            text = idleText.room11B

        elseif not sheetAcquired then

            text = idleText.room11A

        end

    elseif self.room == "room6" and self.name == "otaku" then

        text = idleText.room6o

    else

        text = idleText[self.room] or { }

    end

    return text

end


function npc:nextCheck()

    if self.room == "room10" then

        if not atticOpened and self.textStep == 3 then

            openAttic()

        end

    elseif self.room == "room11" then

        if not sheetAcquired and self.textStep == 5 then

            takeSheets()

        end
    end
end


function openAttic()

    atticOpened = true

    for key, val in pairs(maps.room10.objects) do

        if val.name == "Hatch" then

            val.image = gfx.hatchOpen

            val.name = "Ladder"
            val.desc = "Leads to the attic"

        end
    end
end


function takeSheets()

    crystal.item = "Sheets"

    sheetAcquired = true

    for key, val in pairs(maps.room11.objects) do

        if val.name == "Bed" then

            val.image = gfx.bedEmpty

            val.name = "Bed Frame"
            val.desc = "Looks kinda lonely"

        end
    end
end
