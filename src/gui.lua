gui = { }


gui.create = function()

    hoverText = ""

    talk = nil

    menuButton = {
        name = "menu",
        text = "Open Settings",
        x = 240,
        y = 0,
        w = 16,
        h = 16
    }

    mapButton = {
        name = "map",
        text = "Open Map",
        x = 168,
        y = 160,
        w = 88,
        h = 32
    }

    gui.boxFlash = 0

    mapBoxes = {
        {   -- Otaku room
            x = 28, y = 26, w = 14, h = 3
        },
        {   -- Basement
            x = 45, y = 26, w = 14, h = 3
        },
        {   -- Dining room
            x = 11, y = 20, w = 14, h = 3
        },
        {   -- Main hall
            x = 28, y = 20, w = 14, h = 3
        },
        {   -- Entrance hall
            x = 45, y = 20, w = 14, h = 3
        },
        {   -- Kitchen
            x = 11, y = 14, w = 14, h = 3
        },
        {   -- Living room
            x = 45, y = 14, w = 14, h = 3
        },
        {   -- Library
            x = 62, y = 14, w = 14, h = 3
        },
        {   -- Balcony
            x = 11, y = 8, w = 14, h = 3
        },
        {   -- Second floor hall
            x = 28, y = 8, w = 14, h = 3
        },
        {   -- Bedroom
            x = 45, y = 8, w = 14, h = 3
        },
        {   -- Study
            x = 11, y = 2, w = 14, h = 3
        },
        {   -- Bathroom
            x = 45, y = 2, w = 14, h = 3
        },
        {   -- Attic
            x = 28, y = 2, w = 14, h = 3
        }
    }

    for k, v in ipairs(mapBoxes) do

        v.x = v.x - 3

    end

    talkH = { }
    talkHTimer = 0

    coBox = {
        x = 224,
        y = 0,
        w = 16,
        h = 16
    }

end


function addHText(text)

    table.insert(talkH, text)

end


gui.update = function(dt, singleClick)

    -- Hovering of menu button
    if math.overlap(guiCursor, menuButton) then

        hoverText = menuButton.text

        if singleClick then

            state.load(states.menu)

        end
    elseif math.overlap(guiCursor, mapButton) then

        hoverText = mapButton.text

        if singleClick then

            state.load(states.map)

        end

    elseif math.overlap(guiCursor, coBox) and currentObject then

        if singleClick then
            dropItem()
        end
    end
end


function dropItem()

    currentObject.x = player.x + 18 - currentObject.w / 2
            currentObject.y = 128 - currentObject.h

    table.insert(objects, currentObject)

    currentObject = nil

end

gui.draw = function()

    -- Mouse box for click collisions
    mouseBox = {
        x = gameCursor.x + view.x,
        y = gameCursor.y,
        w = 1,
        h = 1
    }

    infoDrawn = false

    -- Draw black bars
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)
    g.rectangle('fill', 0, 0, 256, 16)

    local oName, oDesc = "", ""

    for key, obj in ipairs(objects) do

        if math.overlap(mouseBox, obj) then

            oName = obj:getName()
            oDesc = obj:getDescription()

        end
    end

    if oName ~= "" then

        infoBox(nil, oName, oDesc, nil)

    end

    -- Draw tool buttons
    g.setColor(255, 255, 255)
    if currentObject then
        g.draw(currentObject.icon, 224, 0)
        if math.overlap(guiCursor, coBox) then
            infoBox(nil, currentObject:getName(),
                         currentObject:getDescription(), nil)

            hoverText = "Click to drop item"
        end
    end

    g.setColor(255, 255, 255)
    g.draw(gfx.minimap, 192, 160)
    for key, npc in ipairs(npcs) do

        local box, mode =
            {
                x = 28 * (key - 1),
                y = 160,
                w = 28,
                h = 32
            },
            "Alive"

        if not npc:isAlive() then

            mode = "Dead"

        elseif npc:isEscaped() then

            mode = "Escaped"

        end

        -- Draw portrait
        g.setColor(255, 255, 255)
        g.draw(gfx.icons.people[npc.name][mode:lower()], box.x+4, box.y+4)

        local name = npc.gName

        if npc.gName == "Otaku" and mode == "Escaped" then

            name = "???"
            mode = "Unknown"

        end

        if math.overlap(guiCursor, box) then

            infoBox(npc.colour, name, "Item: " .. npc:getItemName(), mode)

        end

    end

    g.setColor(50, 175, 250, 205 + math.cos(gui.boxFlash * math.pi / 180) * 50)
    local box = mapBoxes[tonumber(map.current.name:sub(5))]
    g.rectangle('fill', 192 + box.x, 160 + box.y, box.w, box.h)

    g.setColor(255, 255, 255)
    g.draw(gfx.icons.settings, menuButton.x, menuButton.y)

    if #talkH > 0 then

        hoverText = talkH[1]

    end

    -- Draw hover text
    g.setFont(fnt.small)
    g.setColor(255, 255, 255)
    g.print(hoverText, 2, 2)
    g.setFont(fnt.smallre)
    g.setColor(0, 0, 0)
    g.print(hoverText, 2, 2)
    g.setFont(fnt.small)

    -- If someone is talking
    if (talk) then

        -- Draw text speaker
        g.setColor(talk.colour)
        g.setFont(fnt.small)
        g.print(talk.speaker, 2, 146)
        g.setColor(0, 0, 0)
        g.setFont(fnt.smallre)
        g.print(talk.speaker, 2, 146)

        -- Draw text
        g.setColor(255, 255, 255)
        g.setFont(fnt.small)
        g.printf(talk.text, 2, 162, 252, 'left')

    else



    end

end


function infoBox(colour, title, text, right)

    if not infoDrawn then

        colour = colour or {255, 255, 255}
        title = title or ""
        text = text or ""
        right = right or ""

        g.setColor(0, 0, 0, 192)

        g.rectangle('fill', 0, 128, 172, 32)

        g.setColor(colour)
        g.setFont(fnt.small)
        g.print(title, 2, 130)

        g.setColor(0, 0, 0)
        g.setFont(fnt.smallre)
        g.print(title, 2, 130)

        g.setFont(fnt.small)
        g.setColor(255, 255, 255)
        g.printf(right, 2, 130, 168, 'right')

        g.print(text, 2, 146)

        infoDrawn = true

    end
end
