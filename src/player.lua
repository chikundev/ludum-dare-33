-- chikun :: 2015
-- Player script



player = {}



-- Called when map loads
player.set = function(x, y, w, h)


    -- Set player position
    player.x = x ; player.y = y
    player.w = w ; player.h = h

    player.facing = 1

    player.imageTable = {
        gfx.bird.still,
        gfx.bird.up,
        gfx.bird.still,
        gfx.bird.down
    }

    player.imageStep = 1

    -- Set previous positioning
    player.xPrevious = player.x
    player.yPrevious = player.y

    -- Set player image
    player.image = gfx.bird.still

    -- For player movement
    player.gravity      = 2048
    player.ySpeed       = 0
    player.ySpeedMax    = 768

    -- Set player score
    player.score = 0


end



-- Called using update phase
player.update = function(dt)

    player.imageStep = player.imageStep + dt * 7

    if player.imageStep >= (#player.imageTable + 1) then

        player.imageStep = player.imageStep - #player.imageTable

    end

    if not isOUYA then
        -- Does player have a place to go?
        if (player.destination) then


            local standX, standY =
                player.x + 6,
                player.y + 18

            -- Determine position to move to
            local posXTo, posYTo =
                player.destination.x -15,
                player.destination.y +15

            if player.destination.type ~= "click" then

                posYTo = 48

            end

            newFlag = false

            local pathClear = true
            local slopeX, slopeY =
                posXTo - standX,
                posYTo - standY

            for i = 0, 4 do

                if math.overlapTable(collisions, {
                            x = standX + slopeX * (i / 16),
                            y = standY + slopeY * (i / 16),
                            w = 1,
                            h = 1
                        }) then
                    pathClear = false
                end

            end

            -- Determine angle to move along
            local angle = math.atan2((posYTo - player.y),
                        (posXTo - player.x))


            -- If player is close to destination or at same position as last time...
            if (math.dist(player.x, player.y, posXTo, posYTo) < 4) then

                -- If a click...
                if player.destination.type == "click" then

                    -- Set player's position to destination
                    player.x = posXTo ; player.y = posYTo

                    if player.destination.to and
                       not (player.destination.to == "room14" and not atticOpened)then

                        dest = nil

                        if not ((player.destination.door == "stairLeft" or
                           player.destination.door == "stairRight") and
                            map.current.name ~= "room2") then

                            dest = map.current.name

                        end

                        map.change(maps[player.destination.to], dest)

                    elseif player.destination.object then

                        for key, object in ipairs(objects) do

                            if object.name == player.destination.object then

                                if object:isMoveable() then

                                    if currentObject then

                                        dropItem()

                                    end

                                    if not currentObject then

                                        if object.name == "Knives" then

                                            currentObject = objectTypes.knife()

                                            table.insert(objects,
                                                objectTypes.knifeRack(
                                                object.x,
                                                object.y,
                                                object.w,
                                                object.h))

                                            table.remove(objects, key)

                                        else

                                            currentObject = object

                                            table.remove(objects, key)
                                        end
                                    end
                                elseif currentObject then

                                    currentObject:use(object)

                                end
                            end
                        end
                    end
                end

                -- Nullify destination
                player.destination = nil

            else

                -- Actually move player
                player.x = player.x + math.cos(angle) * dt * 160

                while math.overlapTable(collisions, player) do
                    player.x = math.round(player.x - math.sign(math.cos(angle)))
                end

                player.y = player.y + math.sin(angle) * dt * 96

                while math.overlapTable(collisions, player) do
                    player.y = math.round(player.y - math.sign(math.sin(angle)))
                end

            end
        end
    else

    end


    -- Constrain player movement to a rectangle
    player.x = math.clamp(-18,
                    player.x,
                    map.current.w * map.current.tileW - 18)

    if math.sign(player.x - player.xPrevious) ~= 0 then
        player.facing = math.sign(player.x - player.xPrevious)
    end

    -- Update previous position
    player.xPrevious = player.x
    player.yPrevious = player.y


end



-- Called using draw phase
player.draw = function()

    g.setColor(255, 255, 255, 192)

    local scale = 1 - (math.abs(player.y - 144)) / 288

    g.draw(player.imageTable[math.floor(player.imageStep)], player.x + 20,
        player.y, 0, scale * -player.facing, scale, 20, 22)

    g.setColor(255, 255, 255)

end
