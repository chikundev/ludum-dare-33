local battery = class(Object, function(battery, x, y, w, h)

        local name, desc, icon, image =
            "Car Battery",
            "Shockingly powerful",
            gfx.icons.battery,
            gfx.battery

        Object.init(battery, x, y, w, h, name, desc, icon, image, true)

    end)

function battery:use(other)

    if other.name == "Lamp" and not batterySet then

        other.image = gfx.lampPlugged

        other.desc = "Battery included"

        currentObject = nil

        batterySet = true

    end
end

objectTypes.battery = battery
