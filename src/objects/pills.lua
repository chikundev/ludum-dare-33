local pills = class(Object, function(pills, x, y, w, h)

        local name, desc, icon, image =
            "Pills",
            "Probably prescription",
            gfx.icons.pills,
            gfx.pills

        Object.init(pills, x, y, w, h, name, desc, icon, image, true)

    end)

function pills:use(other)

    if other.name == "Microwave" and not pillsDone then

        pillsDone = true

        addHText("You slip the pills into the ramen")

        currentObject = nil

    end
end

objectTypes.pills = pills
