Object = class(function(newObject, x, y, w, h, name, desc, icon, image, pu)

        newObject.x = x ; newObject.y = y
        newObject.w = w ; newObject.h = h

        newObject.name = name
        newObject.desc = desc
        newObject.icon = icon
        newObject.image = image

        newObject.pu = pu       -- Can be picked up?

    end)

function Object:getName()

    return self.name

end

function Object:getDescription()

    return self.desc

end

function Object:isMoveable()

    return self.pu

end

function Object:use()

end

objectTypes = {

    balcony = class(Object, function(balcony, x, y, w, h)

            local name, desc, icon, image =
                "Balcony",
                "Very high up",
                nil,
                gfx.balconyRail

            Object.init(balcony, x, y, w, h, name, desc, icon, image, false)

        end),

    bed = class(Object, function(bed, x, y, w, h)

            local name, desc, icon, image =
                "Bed",
                "Looks unused",
                nil,
                gfx.bedSheets

            Object.init(bed, x, y, w, h, name, desc, icon, image, false)

        end),

    chandelier = class(Object, function(chandelier, x, y, w, h)

            local name, desc, icon, image =
                "Chandelier",
                "Looks fancy",
                nil,
                gfx.chandelier

            Object.init(chandelier, x, y, w, h, name, desc, icon, image, false)

        end),

    hatch = class(Object, function(hatch, x, y, w, h)

            local name, desc, icon, image =
                "Hatch",
                "To the attic",
                nil,
                gfx.hatchClosed

            Object.init(hatch, x, y, w, h, name, desc, icon, image, false)

        end),

    knifeRack = class(Object, function(knifeRack, x, y, w, h)

            local name, desc, icon, image =
                "Knife Rack",
                "Held a knife",
                nil,
                gfx.knivesTaken

            Object.init(knifeRack, x, y, w, h, name, desc, icon, image, false)

        end),

    knives = class(Object, function(knives, x, y, w, h)

            local name, desc, icon, image =
                "Knives",
                "Great for cutting things",
                gfx.icons.knife,
                gfx.knives

            Object.init(knives, x, y, w, h, name, desc, icon, image, true)

        end),

    lamp = class(Object, function(lamp, x, y, w, h)

            local name, desc, icon, image =
                "Lamp",
                "It needs power",
                nil,
                gfx.lampUnplugged

            Object.init(lamp, x, y, w, h, name, desc, icon, image, false)

        end),

    microwave = class(Object, function(microwave, x, y, w, h)

            local name, desc, icon, image =
                "Microwave",
                "Ramen's a'cookin'",
                nil,
                gfx.microwave

            Object.init(microwave, x, y, w, h, name, desc, icon, image, false)

        end),

    otakuDoor = class(Object, function(door, x, y, w, h)

            local name, desc, icon, image =
                "Locked Door",
                "Something's inside",
                nil,
                gfx.doorClosed2

            Object.init(door, x, y, w, h, name, desc, icon, image, false)

        end)
}

require(... .. "/battery")
require(... .. "/chair")
require(... .. "/knife")
require(... .. "/lard")
require(... .. "/pills")
require(... .. "/scissors")
