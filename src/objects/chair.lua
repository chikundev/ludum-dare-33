local chair = class(Object, function(chair, x, y, w, h)

        local name, desc, icon, image =
            "Chair",
            "Wooden and sturdy",
            gfx.icons.chair,
            gfx.chair

        Object.init(chair, x, y, w, h, name, desc, icon, image, true)

    end)

function chair:use(other)

    if other.name == "Locked Door" and not chairSet then

        other.image = gfx.doorBlocked

        other.desc = "Blocked by chair"

        currentObject = nil

        chairSet = true

    end

end

objectTypes.chair = chair
