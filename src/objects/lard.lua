local lard = class(Object, function(lard, x, y, w, h)

        local name, desc, icon, image =
            "Lard",
            "A greasy bucket of lard",
            gfx.icons.lard,
            gfx.lard

        Object.init(lard, x, y, w, h, name, desc, icon, image, true)

    end)

function lard:use(other)

    local inAttic = false

    for key, npc in ipairs(npcs) do

        if npc.room == "room14" and npc:isAlive() then

            inAttic = true

        end
    end

    if inAttic and other.name == "Ladder" and not ladderGreased then

        currentObject = nil

        other.image = gfx.hatchSticky

        ladderObj = other

        ladderGreased = true

    end
end

objectTypes.lard = lard
