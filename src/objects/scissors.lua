local scissors = class(Object, function(scissors, x, y, w, h)

        local name, desc, icon, image =
            "Scissors",
            "Tiny cutters",
            gfx.icons.scissors,
            gfx.scissors

        Object.init(scissors, x, y, w, h, name, desc, icon, image, true)

    end)

function scissors:use(other)

    cut(other)

end

objectTypes.scissors = scissors
