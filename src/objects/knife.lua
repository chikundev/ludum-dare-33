local knife = class(Object, function(knife)

        local name, desc, icon, image =
            "Knife",
            "Great for cutting things",
            gfx.icons.knife,
            gfx.icons.knife

        Object.init(knife, 0, 0, 16, 16, name, desc, icon, image, true)

    end)

function knife:use(other)

    cut(other)

end

objectTypes.knife = knife


function cut(other)

    local NPCinPlace = false

    for key, npc in ipairs(npcs) do

        if npc.room == "room3" and
           npc.x == roomStandPos["room3"].x and
           npc.y == roomStandPos["room3"].y then

            NPCinPlace = true
        end
    end

    if other.name == "Chandelier" and not chanFallen and NPCinPlace then

        other.image = gfx.chandelierWrecked

        other.desc = "Smashed to bits"

        other.y = 88

        currentObject = nil

        chanFallen = true

    elseif other.name == "Balcony" and not sheetsCut and time.hour == 2 and
           not crystal:isEscaped() then

        other.image = gfx.balconyRailSheetCut

        other.desc = "Sheets were cut"

        currentObject = nil

        sheetsCut = true

    end

end
