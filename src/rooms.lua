roomNames = {
    room1 = "Otaku's Room",
    room2 = "Basement",
    room3 = "Dining Room",
    room4 = "Main Hall",
    room5 = "Entrance Hall",
    room6 = "Kitchen",
    room7 = "Living Room",
    room9 = "Balcony",
    room10 = "Second Floor Hall",
    room11 = "Bedroom",
    room12 = "Library",
    room13 = "Bathroom",
    room14 = "Attic"
}


roomStandPos = {
    room2 = { x = 64, y = 128 },
    room3 = { x = 248, y = 140 },
    room5 = { x = 464, y = 128 },
    room6 = { x = 264, y = 132 },
    room7 = { x = 128, y = 128 },
    room9 = { x = 128, y = 112 },
    room10 = { x = 384, y = 128 },
    room11 = { x = 256, y = 128 },
    room12 = { x = 384, y = 128 },
    room13 = { x = 128, y = 128 },
    room14 = { x = 128, y = 128 }
}
