-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

    inScreen = 0

end


-- On state update
function newState:update(dt)

    inScreen = math.min(inScreen + dt, 1)

end


-- On state draw
function newState:draw()

    g.setColor(255, 255, 255, 255 * inScreen)
    g.draw(gfx.splash.gameOver, 0, 0)

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
