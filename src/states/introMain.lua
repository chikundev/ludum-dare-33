-- chikun :: 2014
-- intro state


-- Temporary state, removed at end of script
local introState = { }


-- On state create
function introState:create()

    introText = {
        { 1, "Alright, this is how we're going to do things." },
        { 1, "Every hour from now, we're going to split up." },
        { 1, "Each of us is going to check out a room separately." },
        { 1, "Then, at the end of the hour, we come back to this room..." },
        { 1, "And discuss anything we've found. Sound good?" },
        { 2, "Yep." },
        { 4, "Totally." },
        { 5, "Sure." },
        { 3, "Whatever." },
        { 1, "OK, great! See you all here at 1." }
    }

    introStep = 1

    introShow = 0

    map.swap(maps.room4)

end


-- On state update
function introState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then
            state.current = states.play
            time:startHour()
            bgm.title:stop()
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    for i = 1, 5 do

                        npcs[i].room = "room4"

                    end

                    states.play:create()
                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }
                    states.play:update(dt)

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end
end


-- On state draw
function introState:draw(noFade)


    for key = 1, 5 do

        npc = npcs[key]

        npc.room = "room4"

        if inMain then
            npc.x = 320 + (128 * ((key - 1) / 4))
            npc.y = 128
        end

        npc.sequence = { }
    end

    local yOffset = 0
    g.setColor(255, 255, 255)

    g.translate(-256, 16)
    map.draw(maps.room4)

    g.setLineStyle('rough')

    local hdeg = (270) * math.pi / 180

    g.setColor(0, 0, 0)
    g.line(384, 48.5, 384 + math.cos(hdeg) * 4, 48.5 + math.sin(hdeg) * 4)
    g.line(384, 48.5, 384, 40.5)
    for key, npc in ipairs(npcs) do

        npc:draw()

    end
    g.translate(256, -16)

    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    local text = introText[introStep][2]

    local key, npc =
        introText[introStep][1],
        npcs[introText[introStep][1]]
    g.setColor(npc.colour)
    g.setFont(fnt.small)
    g.print(npc.gName, 2, 146)
    --g.circle('fill', 64 + (128 * (key - 1) / (countHouseNPC() -1)), 64, 2)
    g.setColor(0, 0, 0)
    g.setFont(fnt.smallre)
    g.print(npc.gName, 2, 146)

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')

    if fade and not noFade then

        if fade >= 0.5 then

            states.play:draw()
        end

        g.setColor(255, 255, 255, 255 - math.abs(fade - 0.5) * 510)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end
end


-- On state kill
function introState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return introState
