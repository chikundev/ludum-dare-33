-- chikun :: 2015
-- DEATH BY LADDER

-- Temporary state, removed at end of script
local deathLadderState = { }


-- On state create
function deathLadderState:create()

    introText = {
        { "G", "Let me just get down here..." },
        { "G", "WHOAAA!" },
        { "", "The sound of a loud *THUD* permeates the hall." },
        { "Ghost Duck", "Wehk. Wehk. Wehk." },
        { "", "You decide to hide the body in the attic." },
    }

    fade = nil

    introStep = 1

    introShow = 0

    love.audio.stop()

    bgm.doa:play()

    ladderObj.image = gfx.hatchStickyBroken

end


-- On state update
function deathLadderState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then

            player.x = 360
            player.y = 48

            bgm.doa:stop()
            resetNPCPositions()
            state.change(states.meeting)
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end

end


-- On state draw
function deathLadderState:draw()


    local bg = gfx.splash.death.ladder["4"]

    if introStep < 4 then
        bg = gfx.splash.death.ladder[time.deathYet:lower() .. introStep]
    end

    local text = introText[introStep][2]

    local yOffset = 0
    g.setColor(255, 255, 255)
    g.draw(bg, 0, 16 + yOffset)


    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')


    local key = introText[introStep][1]

    local name = key

    if key == "G" then

        local npc = npcs[2]

        name = npc.gName

        g.setColor(npc.colour)

    end

    g.setFont(fnt.small)
    g.print(name, 2, 146)

    g.setColor(0, 0, 0)
    g.setFont(fnt.smallre)
    g.print(name, 2, 146)

    if fade then

        g.setColor(0, 0, 0, 255 * fade)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end

end


-- On state kill
function deathLadderState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return deathLadderState

