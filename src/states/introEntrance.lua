-- chikun :: 2014
-- intro state


-- Temporary state, removed at end of script
local introState = { }


-- On state create
function introState:create()
    map.swap(maps.room5)

    introText = {
        { 1, "Okay, we're in. Let's get to investigating this place." },
        { 2, "Amy, wait a second." },
        { 1, "What?" },
        { 4, "Uh..." },
        { 2, "Sky, did you seriously close the door behind us?" },
        { 5, "Yeah, why?"},
        { 2, "The door locked itself, Skylar."},
        { 5, "What?"},
        { 3, "Are you serious, Chiyo?"},
        { 2, "You locked us in! Now we're going to have to find another way " ..
             "out."},
        { 5, "Ugh! Skylar, this is just so typical!"},
        { 1, "Calm down! It's not going to be a problem."},
        { 1, "We'll search the house and then just go through a window. " ..
             "No problem."},
        { 5, "I don't see any windows..."},
        { 1, "Just... Don't freak out, okay?"},
        { 4, "This place is creepy enough! Remember how it's " ..
             "haunted?"},
        { 5, "Melanie, that's just a myth. There's nothing to be afraid of."},
        { 5, "And there's no way we're going to be trapped in here."},
        { 1, "Exactly."},
        { 1, "Well, I guess we should look around..."}
    }

    introStep = 1

    introShow = 0

    girlsIn = false

    into = 0

end


-- On state update
function introState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then
            fade = nil
            states.introMain:create()
            state.current = states.introMain
            bgm.title:stop()
        end

    elseif girlsIn then

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    for i = 1, 5 do

                        npcs[i].room = "room4"

                    end

                    states.introMain:create()
                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    else

        into = into + dt * 2

        for i = 1, 5 do

            local npc = npcs[i]

            npc.room = "room5"

            if into < i then
                npc.x = 496
                npc.y = 128
            end

            npc.sequence = { }
            npc.goal = {
                x = 320 + (128 * (i - 1) / 5),
                y = 128
            }

            npc:update(dt)

            if npc.step == 0 then

                if i < 4 then

                    npc.facing = 1

                elseif i == 5 then

                    girlsIn = true

                end
            end
        end
    end
end


-- On state draw
function introState:draw(noFade)

    local yOffset = 0
    g.setColor(255, 255, 255)

    g.translate(-256, 16)
    map.draw(maps.room5)
    for i = 1, 5 do

        npcs[i]:draw(math.clamp(0, into - (i - 1), 1))

    end
    g.translate(256, -16)

    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    if girlsIn then

        local text = introText[introStep][2]

        local key, npc =
            introText[introStep][1],
            npcs[introText[introStep][1]]
        g.setColor(npc.colour)
        g.setFont(fnt.small)
        g.print(npc.gName, 2, 146)
        --g.circle('fill', 64 + (128 * (key - 1) / 5), 64, 2)
        g.setColor(0, 0, 0)
        g.setFont(fnt.smallre)
        g.print(npc.gName, 2, 146)

        -- Draw intro text
        g.setColor(255, 255, 255)
        g.setFont(fnt.small)
        g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')
    end

    if fade and not noFade then

        if fade >= 0.5 then

            inMain = true

            states.introMain:draw(true)
        end

        g.setColor(255, 255, 255, 255 - math.abs(fade - 0.5) * 510)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end
end


-- On state kill
function introState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return introState
