-- chikun :: 2014
-- intro state


-- Temporary state, removed at end of script
local introState = { }


-- On state create
function introState:create()
    map.swap(maps.room4)

    introText = {
        { "", "Tonight, there are five girls who enter this house." },
        { "", "Whether they are here for a dare, to investigate..." },
        { "", "Their motive is unimportant." },
        { "", "Follow their movements and find ways to prevent their " ..
              "escape."},
        { "", "You must do this before the powers of the spirits wane at " ..
              "dawn."},
        { "", "It is midnight." },
        { "", "You have six hours to succeed." },
        { "", "Do not fail." }
    }

    introStep = 1

    introShow = 0

    into = 0

    plStep = 0

    plTable = {
        gfx.bird.still,
        gfx.bird.up,
        gfx.bird.still,
        gfx.bird.down
    }

end


-- On state update
function introState:update(dt)

    plStep = (plStep + dt * 7) % 4

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then
            fade = nil
            states.introEntrance:create()
            state.current = states.introEntrance
            bgm.title:stop()
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    states.introEntrance:create()
                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end
end


-- On state draw
function introState:draw(noFade)

    local yOffset = 0
    g.setColor(255, 255, 255)

    g.translate(-256, 16)
    map.draw(maps.room4)

    g.setLineStyle('rough')

    local hdeg = (270) * math.pi / 180

    g.setColor(0, 0, 0)
    g.line(384, 48.5, 384 + math.cos(hdeg) * 4, 48.5 + math.sin(hdeg) * 4)
    g.line(384, 48.5, 384, 40.5)

    g.translate(256, -16)

    g.setColor(255, 255, 255)
    g.draw(plTable[math.floor(plStep + 1)], 160, 64)

    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    local text = introText[introStep][2]

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')

    if fade and not noFade then

        if fade >= 0.5 then

            states.introEntrance:draw(true)
        end

        g.setColor(255, 255, 255, 255 - math.abs(fade - 0.5) * 510)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end
end


-- On state kill
function introState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return introState
