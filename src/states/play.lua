-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- On play state create
function playState:create()


    oldBGM = bgm.loop

    -- Create the GUI
    gui.create()


    -- Set our map to the test map
    map.swap(maps.room4)


    -- Create our view object
    view = {
        x = 0,
        y = 0,
        w = 256,
        h = 144
    }


    -- Create our gameScreen object
    gameScreen = {
        x = 0,
        y = 16,
        w = 256,
        h = 144
    }

    deathRooms = { }

   --bgm.hour1:play()


end


-- On play state update
function playState:update(dt)

    if #talkH > 0 then

        talkHTimer = talkHTimer + dt

        if talkHTimer >= 2 then

            talkHTimer = 0

            table.remove(talkH, 1)

        end
    end

    gui.boxFlash = (gui.boxFlash + (360 * dt)) % 360

    -- Empty hover text
    hoverText = roomNames[map.current.name] or ""

    -- Update player
    player.update(dt)

    -- Run current tool
    tools[currentTool](dt)

    for key, girl in ipairs(npcs) do

        girl:update(dt)

    end

    -- Update view to follow player, but restrict it based on map data
    view.x = math.clamp(0, player.x + 15 - view.w / 2,
        map.current.w * map.current.tileW - view.w)
    view.y = math.clamp(0, player.y + player.h / 2 - view.h / 2,
        map.current.h * map.current.tileH - view.h)

    time:update(dt)

end


-- On play state draw
function playState:draw(tip)

    g.setColor(255, 255, 255)


    -- Move drawing position of objects based on view position
    if isOUYA then

        g.translate(-view.x, -view.y)

    else

        g.translate(-view.x, -view.y + 16)

    end

    -- Draw the map
    map.draw()

    for key, object in ipairs(objects) do

        g.draw(object.image, object.x, object.y)

    end

    g.setColor(0, 0, 0)

    if map.current.name == "room4" then

        g.setLineStyle('rough')

        local hdeg, mdeg =
            ((time.hour + (time.minutes / 60)) / 12 * 360 - 90) * math.pi / 180,
            ((time.minutes / 60) * 360 - 90) * math.pi / 180

        g.line(383, 48.5, 383 + math.cos(hdeg) * 4, 48.5 + math.sin(hdeg) * 4)
        g.line(383, 48.5, 383 + math.cos(mdeg) * 8, 48.5 + math.sin(mdeg) * 8)

    end

    g.setColor(255, 255, 255)

    for key, girl in ipairs(npcs) do

        girl:draw()

    end

    if map.current.name == "room9" then

        for key, object in ipairs(objects) do

            g.draw(object.image, object.x, object.y)

        end

    end

    -- Draw player at its position
    player.draw()

    if map.current.name == "room7" then

        g.setColor(0, 0, 0, 192)

        g.rectangle('fill', view.x, 0, 256, 144)

    end

    map.drawFG()

    -- Reset the drawing position
    g.origin()

    -- Scale this shit UP
    g.scale(g.getWidth() / 256, g.getHeight() / 192)

    -- Draw GUI
    if not tip then gui.draw() end


end


-- On play state kill
function playState:kill()


    -- Kill variables
    coins       = nil
    collisions  = nil


end


-- Transfer data to state loading script
return playState
