-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local titleState = { }


-- On state create
function titleState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Set off timer
    timer = 0

    flashTimer = 0

    flashDT = 1

    --bgm.title:play()


end


-- On state update
function titleState:update(dt)


    -- Increase timer
    timer = timer + dt / 2

    if timer >= 0.5 and not go then

        timer = 0.5
        flashTimer = flashTimer + dt * flashDT

        if flashTimer >= 2 then

            flashTimer = flashTimer - 2

        end

    end

    if m.isDown('l') then

        go = true

        flashDT = 2

    end

    -- When one second passes, change state...
    if (timer > 1) then

        -- ...to the play state
        state.change(states.intro)

    -- If over half a second and sound hasn't played...
    elseif (timer >= 0.5 and not soundPlayed) then

        -- ...then play it!

        -- And set variable
        soundPlayed = true

    end

end


-- On state draw
function titleState:draw()

    local bg = gfx.splash.bg

    if (os.time() % 2) == 1 then

        bg = gfx.splash.bg2

    end

    -- Local alpha
    local alpha = (1 - math.abs(timer - 0.5) * 2) * 256


    -- Set colour with alpha
    g.setColor(255, 255, 255, math.max(alpha - 1, 0))

    -- Set font
    g.setFont(fnt.splash)

    local move = 0

    -- Draw logo with formatting
    g.draw(bg, 0, move)


end


-- On state kill
function titleState:kill()


    -- Kill all variables
    timer = nil
    soundPlayed = nil


end


-- Transfer data to state loading script
return titleState
