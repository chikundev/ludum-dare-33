-- chikun :: 2014
-- intro state


-- Temporary state, removed at end of script
local meetingState = { }


-- On state create
function meetingState:create()

    introStep = 1

    introShow = 0

    fade = nil

    housePeeps = { }

    introText = { }

    for i = 2, 5 do

        if npcs[i]:isAlive() and not npcs[i]:isEscaped() then

            table.insert(housePeeps, i)

        end
    end

    if time.deathYet and time.hour ~=5 and #housePeeps > 0 then

        table.insert(introText,
            {1, "Hmm. Where's " .. _G[time.deathYet].gName .. "?"})

        table.insert(introText,
            {housePeeps[math.random(1, #housePeeps)], "No clue."})

        table.insert(introText,
            {housePeeps[math.random(1, #housePeeps)], "I hope she's fine..."})

        table.insert(introText,
            {1, "Yeah... me too."})

    end

    -- Hour is over
    time.hour = (time.hour + 1)

    if time.hour == 13 then time.hour = 1 end

    time.minutes = 0

    bgm.meetup:play()

    if time.hour == 6 then

        introText = {
            { 1, "It's morning." },
            { 1, "Huh. A compartment just opened up on this clock..." },
            { 1, "A key!?" },
            { 1, "I wonder if it's for the front door?" }
        }

    elseif countHouseNPC() == 1 then

        introText = {
            { 1, "Oh..." },
            { 1, "I guess... It's just me..." }
        }

    elseif time.hour == 5 and (otaku:isAlive() and not otaku:isEscaped()) then

        introText = {
            { 6, "Hello ladies." },
            { 6, "Would you kindly get out of my house?" }	,
            { 1, "We're kind of trapped here..." }	,
            { 6, "Fine, fine. I'll unlock the door." },
            { 6, "Let's go." }
        }

    elseif time.hour == 2 then

        table.insert(introText,
            {3, "I found some sheets."})

        table.insert(introText,
            {3, "Maybe we can use them as a rope."})

        table.insert(introText,
            {1, "Good luck with that, Crystal..."})

    elseif time.hour == 1 then

        table.insert(introText,
            {1, "So, I found an attic hatch..."})

        table.insert(introText,
            {2, "I'll go check it out."})

    elseif #introText == 0 then

        table.insert(introText,
            { 1, "We didn't lose anyone else... that's a plus." })

    end

    resetNPCPositions()
end


-- On state update
function meetingState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then

            if time.hour == 4 then

                if not chairSet then
                    addHText("The Otaku has left the basement")
                    addHText("The Otaku has left the basement")
                end
                state.current = states.play
                bgm.title:stop()

            elseif time.hour == 6 or ( time.hour == 5 and (otaku:isAlive() and not otaku:isEscaped()) ) then

                state.change(states.gameOver)

            elseif countHouseNPC() == 1 then

                state.change(states.deathAmy)

            else

                state.current = states.play
                bgm.title:stop()
            end
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    for key = 1, 5 do

                        npcs[key].room = "room4"

                    end
                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }
                    time:startHour()
                    map.swap(maps.room4)
                    states.play:update(dt)

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end
end


-- On state draw
function meetingState:draw(noFade)

    local yOffset = 0
    g.setColor(255, 255, 255)

    g.translate(-256, 16)
    map.draw(maps.room4)

    g.setLineStyle('rough')

    local hdeg = (time.hour / 12 * 360 - 90) * math.pi / 180

    g.setColor(0, 0, 0)
    g.line(384, 48.5, 384 + math.cos(hdeg) * 4, 48.5 + math.sin(hdeg) * 4)
    g.line(384, 48.5, 384, 40.5)
    for key, npc in ipairs(npcs) do

        npc:draw(1, true)

    end
    g.translate(256, -16)

    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    local text = introText[introStep][2]

    local key, npc =
        introText[introStep][1],
        npcs[introText[introStep][1]]
    g.setColor(npc.colour)
    g.setFont(fnt.small)
    g.print(npc.gName, 2, 146)
    --g.circle('fill', 64 + (128 * (key - 1) / 5), 64, 2)
    g.setColor(0, 0, 0)
    g.setFont(fnt.smallre)
    g.print(npc.gName, 2, 146)

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')

    if fade and not noFade then

        g.setColor(0, 0, 0, (fade  *255))
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end
end


-- On state kill
function meetingState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return meetingState
