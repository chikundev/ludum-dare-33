-- chikun :: 2015
-- Fade between meeting and play or back



-- Temporary state, removed at end of script
local fadeMeetingState = { }


-- On state create
function fadeMeetingState:create()

    fadeMeetingVal = 0

    if toMeeting then

        states.meeting:create()
        states.meeting:update(0.1)

    end

end


-- On state update
function fadeMeetingState:update(dt)

    fadeMeetingVal = math.min(fadeMeetingVal + dt, 1)

    if fadeMeetingVal == 1 then

        if toMeeting then

            resetNPCPositions()
            state.set(states.meeting)

        else

            state.set(states.play)

        end

    end

end


-- On state draw
function fadeMeetingState:draw()

    local fade = fadeMeetingVal

    if toMeeting then

        fade = 1 - fade

    end

    g.setColor(255, 255, 255)

    if fade > 0.5 then

        states.play:draw()

    else

        player.x = 360
        player.y = 48

        states.meeting:draw()

    end

    g.setColor(0, 0, 0, (1 - (math.abs(fade - 0.5) * 2)) * 255)

    g.rectangle('fill', 0, 16, 256, 144)

end


-- On state kill
function fadeMeetingState:kill()

end


-- Transfer data to state loading script
return fadeMeetingState
