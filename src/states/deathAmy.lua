-- chikun :: 2015
-- DEATH BY CHANDELIER


-- Temporary state, removed at end of script
local deathAmyState = { }


-- On state create
function deathAmyState:create()

    introText = {
        { "G", "Who... who's there?" },
        { "G", "Oh... oh God, there's something there..." },
        { "G", "What do you want from me?" },
        { "G", "D-don't come near me!" },
        { "", "..." },
        { "", "Wehk." },
        { "G", "You... you've been following us all this time." },
        { "G", "I saw you." },
        { "G", "How can you be responsible for everything?" },
        { "", "..." },
        { "", "Wehk." },
        { "G", "The haunted spirit of the Henderson mansion... is a duck?" },
        { "G", "Why?" },
        { "G", "T-this doesn't make any sense!" },
        { "", "..." },
        { "Ghost Duck", "Wehk." },
        { "G", "No! What are you doing?" },
        { "G", "AIIIEEEEEEEEEEEEEEEEEEE!" },
        { "", "..." },
        { "", "The final scream sealed the fate of those poor souls..." },
        { "", "...who'd dared to enter the Henderson estate." },
        { "", "Their fate became an urban myth... A legend." },
        { "", "All that spoke of this place now only do so in whispers." },
        { "", "And you are listening. Watching them." },
        { "", "Forever, the spirits will protect the mansion from harm." },
        { "", "And they do so through Ghost Duck." },
        { "", "Forever, he watches..." },
        { "", "...and waits for another to enter." },
    }

    fade = nil

    introStep = 1

    introShow = 0

    love.audio.stop()

    bgm.start:play()

end


-- On state update
function deathAmyState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt*0.25

        if fade >= 1 then

            bgm.start:stop()
            state.change(states.credits)
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end

end


-- On state draw
function deathAmyState:draw()

    local gra = 1

    if introStep > 23 then

        gra = 9

    elseif introStep > 16 then

        gra = 8

    elseif introStep > 15 then

        gra = 7

    elseif introStep > 14 then

        gra = 6

    elseif introStep > 11 then

        gra = 5

    elseif introStep > 9 then

        gra = 4

    elseif introStep > 6 then

        gra = 3

    elseif introStep > 4 then

        gra = 2

    end

    local bg = gfx.splash.death.final[tostring(gra)]

    local text = introText[introStep][2]

    local yOffset = 0
    g.setColor(255, 255, 255)
    g.draw(bg, 0, 16 + yOffset)


    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')


    local key = introText[introStep][1]

    local name = key

    if key == "G" then

        local npc = npcs[1]

        name = npc.gName

        g.setColor(npc.colour)

    end

    g.setFont(fnt.small)
    g.print(name, 2, 146)

    g.setColor(0, 0, 0)
    g.setFont(fnt.smallre)
    g.print(name, 2, 146)

    if fade then

        g.setColor(0, 0, 0, 255 * fade)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end

end


-- On state kill
function deathAmyState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return deathAmyState

