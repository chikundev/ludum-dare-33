-- chikun :: 2014
-- intro state


-- Temporary state, removed at end of script
local introState = { }


-- On state create
function introState:create()

    introText = {
        { "", "The year is 198X." },
        { "", "The location is the old Henderson mansion." },
        { "", "It has been decades since it was last occupied." },
        { "", "..." },
        { "", "Or has it?" },
        { "", "Every night, the windows of the mansion light up."},
        { "", "Why is this?" },
        { "", "There are some who say that the previous occupants " ..
              "died violently..." },
        { "", "That their spirits haunt this place, unable to return " ..
              "to the ether." },
        { "", "Yes, this is a haunted place." },
        { "", "Every year, there are intruders to this place." },
        { "", "The spirits inhabit the form of living things to ward " ..
              "them away." },
        { "", "But sometimes fate is cruel." },
        { "Duck", "\"Wehk, wehk!\"" },
        { "", "Tonight, the spirits take the form of a duck." },
        { "", "This spirit duck..." },
        { "", "This ghost duck..." },
        { "", "You are a duck that must fulfil the wishes of the spirits." },
        { "", "Tonight, five women plan to investigate this haunted mansion." },
        { "", "None must leave." },
        { "", "It is your duty to seal the curse for all who enter this " ..
              "place." },
        { "", "Go now, duck. Go and do what must be done." },
        { "", "The intruders must die." },
    }

    introStep = 1

    introShow = 0

end


-- On state update
function introState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then

            fade = nil
            states.introDuck:create()
            state.current = states.introDuck
            bgm.title:stop()
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    states.introDuck:create()
                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end

end


-- On state draw
function introState:draw()

    local text = introText[introStep][2]

    local yOffset = 0
    g.setColor(255, 255, 255)
    local bg = gfx.splash.intro1
    if introStep > 22 or fade then
        bg = gfx.splash.intro6
    elseif introStep > 17 then
        bg = gfx.splash.intro5
    elseif introStep > 13 then
        bg = gfx.splash.intro4
    elseif introStep > 10 then
        bg = gfx.splash.intro3
    elseif introStep > 7 then
        bg = gfx.splash.intro2
    end
    g.draw(bg, 0, 16 + yOffset)

    if not isOUYA then
        -- Draw bottom bar
        g.setColor(0, 0, 0)
        g.rectangle('fill', 0, 160, 256, 32)
    else
        g.setColor(0, 0, 0, 192)
        g.rectangle('fill', 0, 112, 256, 32)
    end

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')
    if isOUYA then
        g.setFont(fnt.smallre)
        g.setColor(0, 0, 0)
        g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')
        g.setColor(255, 255, 255)
    end

    if fade then

        if fade >= 0.5 then

            states.introDuck:draw(true)
        end

        g.setColor(255, 255, 255, 255 - math.abs(fade - 0.5) * 510)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end

end


-- On state kill
function introState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return introState
