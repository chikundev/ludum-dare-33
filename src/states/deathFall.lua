-- chikun :: 2015
-- DEATH BY LADDER

-- Temporary state, removed at end of script
local deathFallState = { }


-- On state create
function deathFallState:create()

    introText = {
        { "", "You hear a scream, then, seconds later, a loud *THUD*." },
        { "", "Looks like someone was using that sheet to escape." },
        { "", "Good thing they were up too high." },
        { "Ghost Duck", "Wehk. Wehk. Wehk." },
    }

    fade = nil

    introStep = 1

    introShow = 0

    love.audio.stop()

    bgm.doa:play()

end


-- On state update
function deathFallState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then

            player.x = 360
            player.y = 48

            bgm.doa:stop()
            resetNPCPositions()
            state.change(states.meeting)
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end

end


-- On state draw
function deathFallState:draw()

    local text = introText[introStep][2]

    local yOffset = 0
    g.setColor(255, 255, 255)

    states.play:draw(true)


    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')


    local key = introText[introStep][1]

    local name = key

    if key == "G" then

        local npc = npcs[3]

        name = npc.gName

        g.setColor(npc.colour)

    end

    g.setFont(fnt.small)
    g.print(name, 2, 146)

    g.setColor(0, 0, 0)
    g.setFont(fnt.smallre)
    g.print(name, 2, 146)

    if fade then

        g.setColor(0, 0, 0, 255 * fade)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end

end


-- On state kill
function deathFallState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return deathFallState

