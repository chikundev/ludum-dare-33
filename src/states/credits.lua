-- chikun :: 2015
-- Credits


-- Temporary state, removed at end of script
local creditsState = { }


-- On state create
function creditsState:create()

    fade = nil

    timer = 0

    love.audio.stop()

    sfx.credits:setVolume(0.5)

    sfx.credits:play()

end


-- On state update
function creditsState:update(dt)

   timer = math.min(timer + dt, 40)

end


-- On state draw
function creditsState:draw()

    g.setColor(255, 255, 255)

    g.draw(gfx.splash["credits" .. (math.floor(timer / 8) + 1)], 0, 0)

end


-- On state kill
function creditsState:kill()

end


-- Transfer data to state loading script
return creditsState

