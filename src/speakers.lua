speakers = {
    danny = {
        name = "Danny Elfman",
        col  = { 255, 100, 200 }
    },
    maskOfAbassi = {
        name = "Mask of Abassi",
        col  = { 0, 0, 255 }
    },
    maskOfYemaya = {
        name = "Mask of Yemaya",
        col  = { 0, 255, 0 }
    },
    maskOfOlorun = {
        name = "Mask of Olorun",
        col  = { 255, 0, 0 }
    },
    blank = {
        name = "",
        col = { 222, 100, 200 }
    },
    medicineBuddha = {
        name = "Medicine Buddha",
        col = { 255, 100, 200 }
    },
    mermaid = {
        name = "?",
        col = { 255, 100, 200 }
    },
    shiva = {
        name = "Shiva",
        col = { 255, 100, 200 }
    },
    senemut = {
        name = "Senemut",
        col = { 255, 100, 200 }
    },
    danny = {
        name = "Portrait of Danny Elfman",
        col = { 255, 100, 200 }
    },
    teakCabinet = {
        name = "A Teak Cabinet",
        col = { 255, 100, 200 }
    },
        peeweeCabinet = {
        name = "Pee-Wee Herman",
        col = { 255, 100, 200 }
    },
    mermaid2 = {
        name = "?",
        col = { 255, 100, 200 }
    },
      mermaid3 = {
        name = "?",
        col = { 255, 190, 290 }
    },
    duck = {
        name = "Rubber Duck",
        col = { 255, 255, 51 }
    },
    jesus = {
        name = "A Statue of Jesus",
        col = { 255, 255, 201 }
    },
    statue = {
        name = "Statue of a Woman",
        col = { 165, 105, 201 }
    },
    cosmicDog = {
        name = "Cosmic Dog",
        col = { 255, 255, 51 }
    },
    chameleon = {
        name = "Incapable Chameleon",
        col = { 22, 250, 50 }
    },
    forestTotem = {
        name = "Forest Totem",
        col = { 82, 250, 30 }
    },
    clippy = {
        name = "Clippy",
        col = { 82, 250, 30 }
    },
    time = {
        name = "World Clock",
        col = { 82, 250, 30 }
    },
    turtle = {
        name = "Turtle",
        col = { 82, 250, 30 }
     },
    dannyWorld = {
        name = "Danny Elfman, God",
        col = { 82, 250, 30 }
     },
}

triggerDialogue = function(file)

    diaFile = dialogue[file]

    state.load(states.dialog)

end
