tools = { }



-- On interaction
tools.interact = function(dt)


    -- What image the cursor will display
    cursorImage = "interact"


    tools.mouseShit()

    -- Perform click functions in-game if mouse is in-game
    if math.overlap(gameScreen, guiCursor) then

        -- Check all objects
        for key, object in ipairs(objects) do

            -- If mouse is touching object
            if math.overlap(object, mouseBox) then

                -- Set cursor based on object
                --cursorImage = obj[map.current.name][object.name].cursor

                -- Set hover text based on object
                --hoverText   = obj[map.current.name][object.name].text

                -- Mouse overlaps
                noOverlap = false


                -- If clicking
                if (singleClick) then

                    sfx.click:play()

                    player.destination = {
                        x = object.x + object.w / 2,
                        y = object.y + object.h / 2 - player.h / 2,
                        type = "click",
                        object = object.name
                    }

                end
            end
        end

        -- Check doors
        for key, door in ipairs(map.current.doors or { }) do

            if math.overlap(mouseBox, door) then

                hoverText = "To " .. roomNames[door.to]


                -- If clicking
                if (singleClick) then

                    sfx.click:play()

                    player.destination = {
                        x = door.x + door.w / 2,
                        y = door.y + door.h / 2 - 15,
                        door = door.name,
                        type = "click",
                        to = door.to
                    }

                end

            end

        end

        if (moving or noOverlap and m.isDown('l')) then

            player.destination = {
                x = mouseBox.x - 6,
                y = mouseBox.y - 18,
                type = "hold"
            }
        end
    else

        -- Check the gui
        gui.update(dt, singleClick)

    end


    -- Reset player destination on hold
    if (not m.isDown('l') and player.destination) then

        if player.destination.type == "hold" then

            player.destination = nil

        end
    end


    -- Clicking variable
    clicking = m.isDown('l')

end




-- On use
tools.use = function(dt)


    -- What image the cursor will display
    cursorImage = "use"


    hoverText = "Use " .. useObject.name .. " on"

    tools.mouseShit()

    noOverlap = true

    -- Perform click functions in-game if mouse is in-game
    if math.overlap(gameScreen, guiCursor) then

        -- Check all objects
        for key, object in ipairs(objects) do

            -- If mouse is touching object
            if math.overlap(object, mouseBox) and noOverlap then

                -- Set hover text based on object
                hoverText = hoverText .. " " .. obj[map.current.name][object.name].text

                -- Mouse overlaps
                noOverlap = false


                -- If clicking
                if (singleClick) then

                    local result = useObject:onUse(object.name)

                    if not result then

                        launchText({
                                { "", "It had no visible effect." }
                            })

                    end

                    sfx.click:play()

                    currentTool = "interact"

                end
            end
        end

        if noOverlap and singleClick then

            currentTool = "interact"

            sfx.click:play()

        end
    else

        -- Check the gui
        gui.update(dt, singleClick)

    end

    if noOverlap then

        hoverText = hoverText .. "..."

    end


    -- Clicking variable
    clicking = m.isDown('l')

end




-- On use
tools.examine = function(dt)


    -- What image the cursor will display
    cursorImage = "examine"


    hoverText = "Examine..."

    tools.mouseShit()

    -- Perform click functions in-game if mouse is in-game
    if math.overlap(gameScreen, guiCursor) then

        noOverlap = true

        -- Check all objects
        for key, object in ipairs(objects) do

            -- If mouse is touching object
            if math.overlap(object, mouseBox) then

                -- Set hover text based on object
                hoverText   = "Examine ".. obj[map.current.name][object.name].text

                -- Mouse overlaps
                noOverlap = false


                -- If clicking
                if (singleClick) then

                    sfx.click:play()

                    currentTool = "interact"

                    launchText(obj[map.current.name][object.name].examine)

                end
            end
        end

        if noOverlap and singleClick then

            currentTool = "interact"

            sfx.click:play()

        end

    else

        -- Check the gui
        gui.update(dt, singleClick)

    end


    -- Clicking variable
    clicking = m.isDown('l')

end







tools.mouseShit = function()

    -- Mouse box for click collisions
    mouseBox = {
        x = gameCursor.x + view.x,
        y = gameCursor.y,
        w = 1,
        h = 1
    }


    -- If starting a click...
    if (m.isDown('l') and not clicking) then

        -- ...set click table
        click = {
            x = mouseBox.x,
            y = mouseBox.y
        }

    end

    -- If clicking mouse
    if (clicking) then

        -- If mouse moved
        if click.x ~= mouseBox.x and click.y ~= mouseBox.y then

            moving = true

        end
    end

    singleClick = false

    -- If ending a click
    if (clicking and not m.isDown('l')) then

        -- If at same position, single click
        if (not moving) then

            singleClick = true

        else

            moving = nil

        end
    end

    noOverlap = true

end
