winChecks = { }

chalices = { }

cOrder = { 3, 2, 4, 1 }


function winChecks.startRoom()

    return item.check("pitcherWine")

end

function winChecks.room1()

    return item.check("crucifix")

end

function winChecks.room2()

    return dannyMet2

end


function winChecks.room3()

    return (spiritHappy1 and spiritHappy2 and spiritHappy3)

end

function winChecks.room4()

    return item.check("cocaine")

end


function winChecks.room5()

    goodTrue = (#chalices == 4)

    for key, value in ipairs(chalices) do

        if value ~= cOrder[key] then
            goodTrue = false
        end

    end

    return (goodTrue)

end

function fillChalice(number)

    if not checkChalice(number) then

        table.insert(chalices, number)

        sfx.chaliceFill:play()

        t.sleep(0.75)

    else

        launchText({
                { "", "The chalice is already filled." }
            })
    end

    if (#chalices == 4) then

        if (not winChecks.room5()) then

            launchText({
                    { "", "You fill the chalices, but nothing happens." }
                })

            chalices = { }
        else

            launchText({
                    { "", "Upon filling the chalices, a small opening appears." },
                    { "", "Inside - a rusty, albeit ornate, iron key." },
                })

            item.collect('key2')

        end
    end
end

function checkChalice(number)

    return (chalices[1] == number or chalices[2] == number or
        chalices[3] == number or chalices[4] == number)
end

function winChecks.room6()

    return cosmicDogFood

end



function winChecks.room7()

    return item.check("pitcher")

end

function winChecks.room8()

    return merCheck1 and merCheck2 and merCheck3

end


function winChecks.room9()

    if (item.getUsed('cocaine') and item.getUsed('crucifix') and
        wineUsed) then

        ritualComplete = true

        return true

    end

    return false

end

function winChecks.room10()

    return item.check('dogFood')

end

function winChecks.room11()

    return clippyTalk

end

function winChecks.room12()

    return (item.check('key1') and item.check('key2'))

end

